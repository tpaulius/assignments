Homework 01
===========

Exercise 01
-
**1.1:**

        $ pwd 
    .../user19/tpaulius
    $cd /afs/nd.edu/user14/csesoft 

**1.2:**

     $ pwd 
     .../user19/tpaulius
     $cd ../../user14/csesoft
    
**1.3:**

    $ cd ~/../../user14/csesoft
    
**1.4:**

    $ cd ~
    $ ln /afs/nd.edu/user14/csesoft csesoft
 Exercise 02
-
    
**2.1:**

     $cp -r /usr/share/pixmaps ~/images

**2.2:** An example return:

     [tpaulius@fitz90 ~]$ symlinks -o ~/images
     dangling: /afs/nd.edu/user19/tpaulius/images/redhat-office.png -> ../icons/System/48x48/apps/redhat-office.png
They were broken because the linked references do not carry over when directories are copied or moved.

**2.3:** Use the `mv` and `time` command
     
     [tpaulius@fitz90 ~]$ time mv images pixmaps
     0.000u 0.001s 0:00.07 0.0%	0+0k 0+0io 0pf+0w

**2.4:**

     [tpaulius@fitz90 /]$ time mv ~/pixmaps /tmp/tpaulius-pixmaps
     0.011u 0.148s 0:04.03 3.7%	0+0k 16+11520io 0pf+0

 This is much slower.  It is moving the directory all the way up the file tree, then down another.

**2.5:**

     [tpaulius@fitz90 /tmp]$ time rm -r /tmp/tpaulius-pixmaps
     0.000u 0.016s 0:00.01 100.0%	0+0k 0+0io 0pf+0w
This is faster than 2.5, but slower than 2.3

Exercise 03
-
**3.1:**  `du -h`

**3.2:**  `ls -lt`

**3.3:**  `ls | wc -l`

**3.4:**  Yes it has a file called `weaver`:

     [tpaulius@student02 ~]$ find /afs/nd.edu/user37/ccl/software/cctools/x86_64 -name 'weaver'
     /afs/nd.edu/user37/ccl/software/cctools/x86_64/redhat6/lib/python2.6/site-packages/weaver
     /afs/nd.edu/user37/ccl/software/cctools/x86_64/redhat6/bin/weaver
     /afs/nd.edu/user37/ccl/software/cctools/x86_64/osx-10.9/lib/python2.7/site-packages/weaver
     /afs/nd.edu/user37/ccl/software/cctools/x86_64/osx-10.9/bin/weaver


**3.5:**

     [tpaulius@student02 x86_64]$ du -h --max-depth=1 | sort -n
     23M     ./osx-10.9
     72M     ./redhat6
     77M     ./redhat5
     171M    .

**3.6:**

     [tpaulius@student02 x86_64]$ cd osx-10.9
     [tpaulius@student02 osx-10.9]$ ls -n
     total 14


**3.7:**

     [tpaulius@student02 x86_64]$ du -h | sort -n | head -1
     1.1M    ./osx-10.9/doc

     
**3.8:**

     [tpaulius@student02 x86_64]$ find . -mtime +30
     ./osx-10.9/include/cctools/chirp_reli.h
     ./osx-10.9/include/cctools/auth_unix.h
     ./osx-10.9/include/cctools/chirp_recursive.h
     ./osx-10.9/include/cctools/ftp_lite.h
     ./osx-10.9/include/cctools/rmonitor_poll.h




Exercise 04
-

**4.1**:  Readers: User and group; Write: User; Execute: Anyone

**4.2:**  a. `chmod 600`
      b. `chmod 700`
      c. `chmod 200`
      b. `chmod 000`
      
**4.3:** The owner can still delete it.

Excersise 05
-
**5.1:**

     [tpaulius@student02 ~]$ fs listacl ~
     Access list for /afs/nd.edu/user19/tpaulius is
     Normal rights:
       nd_campus l
       system:administrators rlidwka
       system:authuser l
       tpaulius rlidwka
       
     [tpaulius@student02 ~]$ fs listacl ~/Private
     Access list for /afs/nd.edu/user19/tpaulius/Private is
     Normal rights:
       system:administrators rlidwka
       tpaulius rlidwka
    
     [tpaulius@student02 ~]$ fs listacl ~/Public
     Access list for /afs/nd.edu/user19/tpaulius/Public is
        Normal rights:
        nd_campus rlk
        system:administrators rlidwka
        system:authuser rlk
        tpaulius rlidwka

These differences determine who has what authorization in each directory.  For example, anyone from nd_campus can access the home, or Public directories, but not Private.        

**5.2:**

     [tpaulius@student02 ~]$ fs listacl /afs/nd.edu/common
     Access list for /afs/nd.edu/common is
     Normal rights:
       nd_campus rl
       system:administrators rlidwka
       system:authuser rl
       
     [tpaulius@student02 common]$ touch tpaulius.txt
     touch: cannot touch `tpaulius.txt': Read-only file system
       
I do not have permission to create a file in the `common` directory.  This is because for the ACL, I am not an listed user.

**5.3:**


     [tpaulius@student02 ~/cse]$ fs setacl ~/cse pbui rl
     [tpaulius@student02 ~/cse]$ fs listacl ~/cse
      Access list for /afs/nd.edu/user19/tpaulius/cse is
      Normal rights:
        nd_campus l
        system:administrators rlidwka
        system:authuser l
        tpaulius rlidwka
        pbui rl
       

Exercise 06
-

**6.1:**   `umask 000`: anyone can read and write

**6.2:**   `umask 022`: user can read and write, everyone else can read

**6.3:**   `umask 044`: user can read and write, everyone else can write

`umask` sets the permissions for any new files created
