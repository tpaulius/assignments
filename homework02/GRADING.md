Homework 02 - Grading
=====================

**Score**: 14.25 / 15

Deductions
----------

-0.25 Should have used I/O redirection for `sftp`:

    sftp remote < commands.txt

-0.5 decode command is missing the `-d` flag and thus the message is incorrect.

Comments
--------
