Homework02
=


Activity 01
=

Question 1
-
     # Create workspace on source machine
       [tpaulius@remote104]$ cd /tmp
       [tpaulius@remote104 /tmp]$ mkdir tpaulius--workspace
       [tpaulius@remote104 /tmp]$ cd tpaulius--workspace
       
       
       
       # generates 10MB file full of random data
       [tpaulius@remote104 --workspace]$ dd if=/dev/random of=10MB bs=10M count=1

       # created 10 hard links to 10MB file
       [tpaulius@remote104 --workspace]$ ln 10 data0
       [tpaulius@remote104 --workspace]$ ln 10 data1
       [tpaulius@remote104 --workspace]$ ln 10 data2
       [tpaulius@remote104 --workspace]$ ln 10 data3
       [tpaulius@remote104 --workspace]$ ln 10 data4
       [tpaulius@remote104 --workspace]$ ln 10 data5
       [tpaulius@remote104 --workspace]$ ln 10 data6
       [tpaulius@remote104 --workspace]$ ln 10 data7
       [tpaulius@remote104 --workspace]$ ln 10 data8
       [tpaulius@remote104 --workspace]$ ln 10 data9
       
       # creates workspace on target machine
       # loged in to different maching: student02
       [tpaulius@student02 ~]$ cd /tmp
       [tpaulius@student02 /tmp]$ mkdir tpaulius--workspace
       
       
       
Question 2
-

       $ du -h
       11M     .
       
   Yes it is suprising because the file was to be 10MB, and ls -l lists 110M
   
   
   Moving files:
   
       $  scp -r tpaulius@remote104.helios.nd.edu:/tmp/tpaulius--workspace tpaulius@student02.cse.nd.edu:/tmp/tpaulius/workspace
   
   On student02:
   
       $ [tpaulius@remote104 tpaulius--workspace]$ ls -l
       total 112640
       -rw-r--r-- 11 tpaulius dip 10485760 Jan 25 13:04 10MB
       -rw-r--r-- 11 tpaulius dip 10485760 Jan 25 13:04 data0
       -rw-r--r-- 11 tpaulius dip 10485760 Jan 25 13:04 data1
       -rw-r--r-- 11 tpaulius dip 10485760 Jan 25 13:04 data2
       -rw-r--r-- 11 tpaulius dip 10485760 Jan 25 13:04 data3
       -rw-r--r-- 11 tpaulius dip 10485760 Jan 25 13:04 data4
       -rw-r--r-- 11 tpaulius dip 10485760 Jan 25 13:04 data5
       -rw-r--r-- 11 tpaulius dip 10485760 Jan 25 13:04 data6
       -rw-r--r-- 11 tpaulius dip 10485760 Jan 25 13:04 data7
       -rw-r--r-- 11 tpaulius dip 10485760 Jan 25 13:04 data8
       -rw-r--r-- 11 tpaulius dip 10485760 Jan 25 13:04 data9

Question 3
-

       [tpaulius@student02 tpaulius--workspace]$ du -h
       111M    .
       
  It is not the same, because when the links were transfers, the indivudal references.  Not the link itself, so there are 11X 10MB files.

Question 4
-
For the `scp` command:

I included the source files, or in this case the entire directory using the `-r` option. Then I listed the source location.


For the `sftp` command:

I logged in on the destination machine.  Started `sftp` and logged in on it.  Navigated to the source and used `get` to take the files.  Then to exit, I exited use `bye`.


For the `rsync` command:

      rsync -avzh /tmp/tpaulius--workspace/10MB tpaulius@student104.cse.nd.edu:/tmp/tpaulius--workspace 
       
Question 5
-

For rsync`, the data is only transfered if it is altered; however, for `scp` and sftp`, the whole set of data is transfered each time it is run.


Question 6
-

I perfer the `rsync` command for the reason that is only syncs the data with the source, instead of transfering the entire set.

Activity02
=

     [tpaulius@remote104 ~]$ nmap -p 9000-10000 -Pn xavier.h4x0r.space

       Starting Nmap 5.51 ( http://nmap.org ) at 2016-01-27 13:11 EST
       Nmap scan report for xavier.h4x0r.space (129.74.161.24)
       Host is up (0.0029s latency).
       Not shown: 997 closed ports
       PORT      STATE    SERVICE
       9097/tcp  open     unknown
       9111/tcp  open     DragonIDSConsole
       9876/tcp  open     sd
       10000/tcp filtered snet-sensor-mgmt

       Nmap done: 1 IP address (1 host up) scanned in 0.17 seconds
       
Accessed the 9876 port.       
       
       
       [tpaulius@remote104 ~]$ curl 129.74.161.24:9876
       _________________________________________
       / Halt! Who goes there?                   \
       |                                         |
       | If you seek the ORACLE, you must query  |
       | the DOORMAN at /{netid}/{passcode}!     |
       |                                         |
       | To retrieve your passcode you must      |
       | decode the file at                      |
       | ~pbui/pub/oracle/${USER}/code using the |
       | BASE64 command.                         |
       |                                         |
       \ Good luck!                              /
       -----------------------------------------
                       \                    ^    /^
                        \                  / \  // \
                         \   |\___/|      /   \//  .\
                          \  /O  O  \__  /    //  | \ \           *----*
                            /     /  \/_/    //   |  \  \          \   |
                            @___@`    \/_   //    |   \   \         \/\ \
                           0/0/|       \/_ //     |    \    \         \  \
                       0/0/0/0/|        \///      |     \     \       |  |
                    0/0/0/0/0/_|_ /   (  //       |      \     _\     |  /
                 0/0/0/0/0/0/`/,_ _ _/  ) ; -.    |    _ _\.-~       /   /
                             ,-}        _      *-.|.-~-.           .~    ~
            \     \__/        `/\      /                 ~-. _ .-~      /
             \____(oo)           *.   }            {                   /
             (    (--)          .----~-.\        \-`                 .~
             //__\\  \__ Ack!   ///.----..<        \             _ -~
            //    \\               ///-._ _ _ _ _ _ _{^ - - - - ~


Decoding the passcode using `base64`:

              [tpaulius@remote104 ~]$ base64 ~pbui/pub/oracle/tpaulius/code
              TXpNMk9HUXhaVFl6TXpWaE1XRXpaRGczTWpKbFpUUmpOVEExWkRrd00yST0K
              
Talking to the sleeper:


        [tpaulius@remote104 oracle]$ SLEEPER &
       [1] 9700
       [tpaulius@remote104 oracle]$  _______________________________
       < Uh... What? What do you want? >
       -------------------------------
        \
         \
         .--.
        |o_o |
        |:_/ |
       //   \ \
       (|     | )
       /'\_   _/`\
       \___)=(___/

       pkill -1 SLEEPER
       [tpaulius@remote104 oracle]$  ________________________________________
       / Hmm... I recognize you tpaulius...     \
       | Here's the message you need to give to |
       | the ORACLE:                            |
       |                                        |
       \ Z2NuaHl2aGY9MTQ1NDA4NTk0Mg==           /
       ----------------------------------------
       \
        \
         .--.
        |o_o |
        |:_/ |
       //   \ \
       (|     | )
       /'\_   _/`\
       \___)=(___/


    
Talking to the oracle:

       Connected to xavier.h4x0r.space (129.74.161.24).
       Escape character is '^]'.
       ________________________
       < Hello, who may you be? >
       ------------------------
        \
         \
             ,;;;;;;;,
            ;;;;;;;;;;;,
           ;;;;;'_____;'
           ;;;(/))))|((\
           _;;((((((|))))
          / |_\\\\\\\\\\\\
     .--~(  \ ~))))))))))))
    /     \  `\-(((((((((((\\
    |    | `\   ) |\       /|)
     |    |  `. _/  \_____/ |
      |    , `\~            /
       |    \  \           /
      | `.   `\|          /
      |   ~-   `\        /
       \____~._/~ -_,   (\
        |-----|\   \    ';;
       |      | :;;;'     \
      |  /    |            |
      |       |            |
       NAME? tpaulius
       ___________________________________
       / Hmm... tpaulius?                  \
       |                                   |
       | That name sounds familiar... what |
       \ message do you have for me?       /
       -----------------------------------
         \
          \
             ,;;;;;;;,
            ;;;;;;;;;;;,
           ;;;;;'_____;'
           ;;;(/))))|((\
           _;;((((((|))))
          / |_\\\\\\\\\\\\
     .--~(  \ ~))))))))))))
    /     \  `\-(((((((((((\\
    |    | `\   ) |\       /|)
     |    |  `. _/  \_____/ |
      |    , `\~            /
       |    \  \           /
      | `.   `\|          /
      |   ~-   `\        /
       \____~._/~ -_,   (\
        |-----|\   \    ';;
       |      | :;;;'     \
      |  /    |            |
      |       |            |
       MESSAGE? Z2NuaHl2aGY9MTQ1NDA4NTk0Mg==
       ______________________________________
       / Ah yes... tpaulius!                  \
       |                                      |
       | You're smarter than I thought. I can |
       | see why the instructor likes you.    |
       |                                      |
       | You met the SLEEPER about 1 minutes  |
       \ ago... What took you so long?        /
       --------------------------------------
        \
         \
             ,;;;;;;;,
            ;;;;;;;;;;;,
           ;;;;;'_____;'
           ;;;(/))))|((\
           _;;((((((|))))
          / |_\\\\\\\\\\\\
     .--~(  \ ~))))))))))))
    /     \  `\-(((((((((((\\
    |    | `\   ) |\       /|)
     |    |  `. _/  \_____/ |
      |    , `\~            /
       |    \  \           /
      | `.   `\|          /
      |   ~-   `\        /
       \____~._/~ -_,   (\
        |-----|\   \    ';;
       |      | :;;;'     \
      |  /    |            |
      |       |            |
       REASON? 1 minute is too long?
       ______________________________________
       / Hmm... Sorry, kid. You got the gift, \
       | but it looks like you're waiting for |
       | something.                           |
       |                                      |
       | Your next life, maybe. Who knows?    |
       \ That's the way these things go.      /
        --------------------------------------
        \
         \
             ,;;;;;;;,
            ;;;;;;;;;;;,
           ;;;;;'_____;'
           ;;;(/))))|((\
           _;;((((((|))))
          / |_\\\\\\\\\\\\
     .--~(  \ ~))))))))))))
    /     \  `\-(((((((((((\\
    |    | `\   ) |\       /|)
     |    |  `. _/  \_____/ |
      |    , `\~            /
       |    \  \           /
      | `.   `\|          /
      |   ~-   `\        /
       \____~._/~ -_,   (\
        |-----|\   \    ';;
       |      | :;;;'     \
      |  /    |            |
      |       |            |
       
       Congratulations tpaulius! You have reached the end of this journey.
              
       I hope you learned something from the ORACLE :]
              
              

       