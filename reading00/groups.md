TLDR - groups
==========

Overview
--------

[groups] print the groups a user is in

Examples
--------
 - [tpaulius@remote104 assignments]$ groups
dip groups: cannot find name for group ID 1100905018
1100905018

Resources
---------

- [groups](http://man7.org/linux/man-pages/man1/groups.1.html)