TLDR - id
==========

Overview
--------

[id] prints the real and effective user and group IDs

Examples
--------

- [tpaulius@remote104 reading00]$ id
uid=191714(tpaulius) gid=40(dip) groups=40(dip),1100905018



