TLDR - who
==========

Overview
--------

[who] show who is logged on

Examples
--------

-[tpaulius@remote104 assignments]$ who
tpaulius pts/0        2016-01-20 20:50 (10.31.6.62)


Resources
---------

- [who](http://man7.org/linux/man-pages/man1/who.1.html)


