TLDR - whoami
==========

Overview
--------

[whoami] print effective userid

Examples
--------

-[tpaulius@remote104 assignments]$ whoami
tpaulius


Resources
---------

- [whoami](http://man7.org/linux/man-pages/man1/whoami.1.html)