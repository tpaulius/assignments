Reading 01 - Grading
====================

**Score**: 2 / 4

Deductions
----------
-1.5 for missing 6 man summaries
-0.5 for no response to question 8 (setenv EDITOR vim/nano/emacs in ~/.cshrc

Comments
--------
