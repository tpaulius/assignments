Reading 02
===========
Question 1
-
    [tpaulius@remote104 ~/cse]$ uname | tee uname.txt
    Linux

Question 2
-
IP address: 129.74.50.77

    [tpaulius@remote104 ~/cse]$ /sbin/ifconfig
    eth0      Link encap:Ethernet  HWaddr 00:50:56:2B:01:04
          inet addr:129.74.50.77  Bcast:129.74.50.255  Mask:255.255.255.0
          inet6 addr: fe80::250:56ff:fe2b:104/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:642525 errors:0 dropped:0 overruns:0 frame:0
          TX packets:333032 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:207349936 (197.7 MiB)  TX bytes:163967332 (156.3 MiB)

    lo        Link encap:Local Loopback
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:174521 errors:0 dropped:0 overruns:0 frame:0
          TX packets:174521 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0
          RX bytes:32514417 (31.0 MiB)  TX bytes:32514417 (31.0 MiB)
          
Question 3
-
    [tpaulius@remote104 ~/cse]$ nslookup xkcd.org
    Server:         66.205.160.99
    Address:        66.205.160.99#53

    Non-authoritative answer:
    Name:   xkcd.org
    Address: 107.6.106.82

Question 4
-

    [tpaulius@remote104 ~/cse]$ nslookup student02@cse.nd.edu
    Server:         66.205.160.99
    Address:        66.205.160.99#53

    ** server can't find student02@cse.nd.edu: NXDOMAIN

    [tpaulius@remote104 ~/cse]$ ping 66.205.160.99
    PING 66.205.160.99 (66.205.160.99) 56(84) bytes of data.
    64 bytes from 66.205.160.99: icmp_seq=1 ttl=61 time=0.571 ms
    64 bytes from 66.205.160.99: icmp_seq=2 ttl=61 time=0.461 ms    
    64 bytes from 66.205.160.99: icmp_seq=3 ttl=61 time=0.536 ms
    64 bytes from 66.205.160.99: icmp_seq=4 ttl=61 time=0.467 ms

Question 5
-

Use the `scp` command.

    $ scp tpaulius@remote104@helios.nd.edu:~/cse/resume.pdf pbui@student02@cse.nd.edu:~/Public/resume.pdf

Question 6
-

    $ tmux
    $ tmux new -s testsesh
    $ tmux detach
    $ tmux a -t testsesh
    


Question 7
-

    [tpaulius@remote104 ~/cse]$ wget http://www3.nd.edu/~tpaulius/resume_10_2015_web.pdf
    --2016-01-24 16:00:21--  http://www3.nd.edu/~tpaulius/resume_10_2015_web.pdf
    Resolving www3.nd.edu... 129.74.12.151
    Connecting to www3.nd.edu|129.74.12.151|:80... connected.
    HTTP request sent, awaiting response... 200 OK
    Length: 119467 (117K) [application/pdf]
    Saving to: âresume_10_2015_web.pdfâ

    100%[======================================>] 119,467     --.-K/s   in 0.003s

    2016-01-24 16:00:21 (43.0 MB/s) - âresume_10_2015_web.pdfâ

    [tpaulius@remote104 ~/cse]$ ls
    resume_10_2015_web.pdf  uname.txt
    
Question 08
-

    [tpaulius@remote104 ~/cse]$ nmap 66.205.160.99

    Starting Nmap 5.51 ( http://nmap.org ) at 2016-01-24 16:03 EST
    Nmap scan report for dns2.nd.edu (66.205.160.99)
    Host is up (0.00060s latency).
    Not shown: 996 filtered ports
    PORT    STATE  SERVICE
    22/tcp  open   ssh
    53/tcp  open   domain
    80/tcp  closed http
    443/tcp closed https

