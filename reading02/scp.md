TLDR - scp
=

Overview
-

`scp` is a command that transfers files between systems

Examples
-

    $ scp source_file destination_fil
    
Resources
-

- [scp] = http://man7.org/linux/man-pages/man1/scp.1.html