# traceroute

> Print the route packets trace to network host.

- Traceroute to a host:

`traceroute {{host}}`
	
	- Disable IP address and host name mapping:

`traceroute -n {{host}}`

- Specify wait time for response: