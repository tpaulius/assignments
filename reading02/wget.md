# wget
	
> Download files from the Web.
> Supports HTTP, HTTPS, and FTP.
	
- Download a URL to a file:
		
`wget -O filename "{{url}}"`
	
- Limit download speed:
	
`wget --limit-rate={{200k}} {{url}}`