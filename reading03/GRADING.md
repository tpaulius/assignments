Reading 03 - Grading
====================

**Score**: 1.5 / 4

Deductions
----------
-0.5 missing one summary page
-2 for incorrect question answers. They should be:

1. strings
2. ldd
3. strace
4. gdb
5. valgrind
6. gprof

Comments
--------
