Reading 03
==========

To view the text in `ls`, use the `strace` command

To find the libaries, 

     $ strace -f man strace
	 
To find the system calls:

	$ strace -e open ls
	
To debug, use the debuggin option in `gcc`, which was invoked with the Makefile, same with the other options.
	
	$ make hello-debug
	
	$ make hello-profile
	
For bottleneck, use the `gprof` to examine the individiual parts of the code and the times with it.
	 
