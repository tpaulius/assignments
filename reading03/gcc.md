gcc - tldr

> Compiles C and C++ source files, then assembles and links them together.

	$ gcc {file} {options} {output}

