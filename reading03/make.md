# make

> Task runner for rules described in Makefile.

- Call the all rule:

`make`

- Call a specific rule:

`make {option}`