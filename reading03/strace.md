# strace

> Traces system calls.

- Start process by its PID:

`strace -p {{pid}}`

- Show the time spent in every system call:

`strace -p {{pid}} -T`